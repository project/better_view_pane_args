# Better View Pane Args

## Overview

This is a user-interface improvement module designed to help users to interact with contextual filter widgets whenever they are displayed as configuration forms on Views Panes.

## Features

Instead of a text input where the user is expected to type in id numbers or machine names, it displays a select list of labels.

## Use cases

1. You need a Views pane to have configuration options when placed on a page, but you want the View pane to include separate filters for use by end-users. To accomplish this, you use contextual filters for the pane configuration, and exposed filters for the end-user filters. This module makes that pane configuration form more user-friendly by converting all those text fields into select lists.

## Support

Current it only acts on these field types:

* taxonomy_term_reference
* entityreference

And these basic Views contextual filter types:

* content type
* language
* has taxonomy term ID

## Configuration

There is no configuration, apart from enabling/disabling the module.

## To do:

* tests
* support more types of fields and/or argument types (please feel free to make requests, as the short list above is sufficient for my needs);
